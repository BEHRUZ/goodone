import Vue from 'vue'
import Router from 'vue-router'
import Test from '../components/Test'

Vue.use(Router)
const router= new Router({
  mode:'history',
  routes:[
    {
      path:'/calculate',
      name:'Calculate',
        component: () =>import('../components/Calculate'),

   },
   {
     path:'/test',
     name:'test',
     component: Test

  }
  ]
})
export default router
