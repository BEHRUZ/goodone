import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    dialogVisible: false,
    baskets:[],
    products: [
      {
        title: "Shef-Burger",
        id: 1,
        category: "Burgers",
        price: "400",
        count: 1,
        remainder: "220",
        image:
          "https://wallbox.ru/wallpapers/main2/201723/149690905959390503d2ece1.20361719.jpg"
      },
      {
        title: "Shef-Burger",
        id: 2,
        count: 1,
        category: "Burgers",
        price: "200",
        remainder: "230",
        image:
          "https://disgustingmen.com/wp-content/uploads/2015/11/add5628bf48f4c4ea9baef88d40d512b.jpg"
      },
      {
        title: "Lavash",
        id: 3,
        category: "Lavash",
        count: 1,
        price: "300",
        remainder: "204",
        image:
          "https://avatars.mds.yandex.net/get-zen_doc/2458644/pub_5f0c0544f22ade06014a2406_5f0c056690379d39c7083c1e/scale_1200"
      },
      {
        title: "Lavash",
        id: 4,
        category: "Lavash",
        price: "400",
        count: 1,
        remainder: "20",
        image:
          "https://avatars.mds.yandex.net/get-zen_doc/2458644/pub_5f0c0544f22ade06014a2406_5f0c056690379d39c7083c1e/scale_1200"
      },
      {
        title: "Pizza",
        id: 5,
        category: "Pizza",
        price: "600",
        count: 1,
        remainder: "20",
        image:
          "https://proprikol.ru/wp-content/uploads/2020/11/kartinki-piczcza-40.jpeg"
      }
    ]
  },
mutations:{
  calcSum() {
    let total = 0
    this.baskets.forEach(item => {
      total += item.price * item.count
    })
    return total
  },
},
  getters: {
    products(state) {
      return state.products;
    },
    dialogVisible(state) {
      return state.dialogVisible;
    },
    baskets(state){
      return state.baskets;
    }
  }
});
export default store;
