import Vue from 'vue'
import App from './App.vue'
import elementUi from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router/index'
import store from './store/store'
import Calculate from './components/Calculate'

Vue.config.productionTip = false
Vue.use(elementUi)
Vue.component('open-window',Calculate)

new Vue({
  router,store,
  render: h => h(App),
}).$mount('#app')
